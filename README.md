# retailys-interview-prokop

## Instalace

nastavit .env.local a .env.test

composer install

php bin/console d:s:c

php bin/console --env=test d:s:c

### Základní testy
php vendor/phpunit/phpunit/phpunit
### PHP stan level 9
php vendor/phpstan/phpstan/phpstan -l9 analyze src/*

Nastavit apache virtual host, případně jiný server

## Použití

Možno buď importovat již připravenou databázi z sql/dump.sql, nebo spustit
/product/import pro stáhnutí a import XML

Výpis počtu produktů - /product/count
Názvy produktů - /product/names
Produkty s náhradními díly - /product/parts

Testováno na Windows 11, Apache 2.4.46, PHP 8.1.15, MySQL 5.7.31

