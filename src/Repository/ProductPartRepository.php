<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @template ProductPart of object
 * @extends  \Doctrine\ORM\EntityRepository<ProductPart>
 */
class ProductPartRepository extends EntityRepository
{
}
