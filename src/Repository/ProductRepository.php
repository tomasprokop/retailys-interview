<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

/**
 * @template Product of object
 * @extends  \Doctrine\ORM\EntityRepository<Product>
 */
class ProductRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function findWithParts()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->leftJoin('p.parts', 'pr')
            ->where('pr.id is not null')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function deleteAll()
    {
        return $this->createQueryBuilder('p')
            ->delete()
            ->getQuery()
            ->execute();
    }
}
