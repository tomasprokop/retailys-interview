<?php

namespace App\Services;

use App\Entity\Product;
use App\Entity\ProductPart;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use ZipArchive;

class Importer
{
    const IMPORT_URL = 'https://www.retailys.cz/wp-content/uploads/astra_export_xml.zip';
    const ZIP_FILE = 'import_file.xml.zip';
    const DIRECTORY = 'extracted';
    const FILE_NAME = 'export_full.xml';

    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function import()
    {
        $this->getRemoteData();
        $this->unzipData();
        $this->parseXML();
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function getRemoteData()
    {
        $ch = curl_init(self::IMPORT_URL);
        if ($ch === false) {
            throw new \Exception('Can\'t init curl');
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $downloadedData = curl_exec($ch);
        curl_close($ch);

        file_put_contents(self::ZIP_FILE, $downloadedData);

        if (file_exists(self::ZIP_FILE) === false) {
            throw new \Exception('No zip file downloaded');
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function unzipData()
    {
        $zip = new ZipArchive;
        $res = $zip->open(self::ZIP_FILE);
        if ($res !== true) {
            throw new \Exception('Can\'t open zip file');
        }
        $zip->extractTo(self::DIRECTORY);
        $zip->close();

        unlink(self::ZIP_FILE);
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function parseXML()
    {
        /** @var ProductRepository<Product> $productRepository */
        $productRepository = $this->entityManager->getRepository(Product::class);
        $productRepository->deleteAll();

        if (file_exists(self::DIRECTORY .'/'. self::FILE_NAME) === false) {
            throw new \Exception('No xml downloaded');
        }

        $content = file_get_contents(self::DIRECTORY .'/'. self::FILE_NAME);
        if ($content === false) {
            throw new \Exception('Can\'t load XML');
        }

        $xml = simplexml_load_string($content);

        if (empty($xml->items)) {
            throw new \Exception('Invalid XML structure');
        }

        $this->parseProducts($xml->items);
        $this->entityManager->flush();

        unlink(self::DIRECTORY .'/'. self::FILE_NAME);
        rmdir(self::DIRECTORY);
    }

    /**
     * @param  \SimpleXMLElement $items
     * @return void
     */
    private function parseProducts(\SimpleXMLElement $items)
    {
        foreach ($items->item as $item) {
            /** @var \SimpleXMLElement $item */
            $product = new Product();
            if (empty($item->attributes())) {
                continue;
            }
            foreach ($item->attributes() as $key => $value) {
                if ($key === 'name') {
                    $product->setName($value);
                }
            }
            if ($item->parts) {
                $this->parseParts($item->parts, $product);
            }
            $this->entityManager->persist($product);
        }
    }

    /**
     * @param  \SimpleXMLElement $parts
     * @param  Product           $product
     * @return void
     */
    private function parseParts(\SimpleXMLElement $parts, Product $product)
    {
        foreach ($parts->part as $part) {
            foreach ($part->item as $item) {
                /** @var \SimpleXMLElement $item */
                if (empty($item->attributes())) {
                    continue;
                }
                foreach ($item->attributes() as $key => $value) {
                    if ($key === 'name') {
                        $part = new ProductPart();
                        $part->setName($value);
                        $part->setProduct($product);
                        $product->addPart($part);
                    }
                }
            }
        }
    }
}
