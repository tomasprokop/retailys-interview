<?php

namespace App\Entity;

use App\Entity\EntityTrait\IdentifiableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    use IdentifiableTrait;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private string $name = '';

    /**
     * @var Collection<int, ProductPart>
     * @ORM\OneToMany(targetEntity="ProductPart", mappedBy="product", cascade={"all"})
     */
    private Collection $parts;

    public function __construct()
    {
        $this->parts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Collection<int, ProductPart>
     */
    public function getParts(): Collection
    {
        return $this->parts;
    }

    /**
     * @param Collection<int, ProductPart> $parts
     */
    public function setParts(Collection $parts): void
    {
        $this->parts = $parts;
    }

    /**
     * @param  ProductPart $part
     * @return void
     */
    public function addPart(ProductPart $part)
    {
        if ($this->parts->contains($part) === false) {
            $this->parts->add($part);
        }
    }

    /**
     * @param  ProductPart $part
     * @return void
     */
    public function removePart(ProductPart $part)
    {
        if ($this->parts->contains($part)) {
            $this->parts->removeElement($part);
        }
    }
}
