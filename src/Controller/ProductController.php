<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use App\Services\Importer;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $_entityManager;
    private Importer $_importer;

    /**
     * @param EntityManagerInterface $entityManager
     * @param Importer               $importer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Importer $importer
    ) {
        $this->_entityManager = $entityManager;
        $this->_importer = $importer;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(path="/product/import", name="app_product_import")
     */
    public function importAction(Request $request)
    {
        try {
            $this->_importer->import();
        } catch (\Exception $e) {
            return (new Response($e->getMessage()))
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(path="/product/count", name="app_product_count")
     */
    public function productCountAction(Request $request)
    {
        $products = $this->_entityManager->getRepository(Product::class)->findAll();

        return $this->render(
            'product/count.html.twig', [
            'count' => count($products)
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(path="/product/names", name="app_product_names")
     */
    public function productNamesAction(Request $request)
    {
        $products = $this->_entityManager->getRepository(Product::class)->findAll();

        return $this->render(
            'product/names.html.twig', [
            'products' => $products
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(path="/product/parts")
     */
    public function productPartsAction(Request $request)
    {
        /**
         * @var ProductRepository<Product> $productRepository
         */
        $productRepository = $this->_entityManager->getRepository(Product::class);
        $products = $productRepository->findWithParts();

        return $this->render(
            'product/parts.html.twig', [
            'products' => $products
            ]
        );
    }
}
