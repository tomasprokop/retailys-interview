<?php

use PHPUnit\Framework\TestStatus\TestStatus;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    public function testProductActiona(): void
    {
        $client = static::createClient();

        $client->request('GET', '/product/count');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $client->request('GET', '/product/names');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $client->request('GET', '/product/parts');
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
}